import EstimateLocators from '../../Locators/EstimateLocators';
import { removeDollar } from '../commands';

export function fillAndAssertLineTotal(data, index, type) {
  cy.get(EstimateLocators[type].name)
    .clear()
    .type(type + ' ' + index)
    .blur();
  let lineTotal = 0.0;
  cy.get(EstimateLocators[type].lineItemTotalAssertValue)
    .last()
    .should('have.value', 0);
  cy.get(EstimateLocators.sideClick).click();
  cy.wait('@updateJob').its('response.statusCode').should('eq', 200);

  // hours
  // cy.get(EstimateLocators[type].hours).should("not.have.class", "Mui-disabled")
  cy.get(EstimateLocators[type].hours).clear().type(data.hours);
  lineTotal = data.hours * 0;
  cy.get(EstimateLocators[type].lineItemTotalAssertValue)
    .last()
    .should('have.value', Math.round(lineTotal * 100) / 100);
  cy.get(EstimateLocators.sideClick).click();
  cy.wait('@updateJob').its('response.statusCode').should('eq', 200);

  // hours * rate
  // cy.get(EstimateLocators[type].rate).should("not.have.class", "Mui-disabled")
  cy.get(EstimateLocators[type].rate).clear().type(data.rate);
  lineTotal = data.rate * data.hours;
  cy.get(EstimateLocators[type].lineItemTotalAssertValue)
    .last()
    .should('have.value', Math.round(lineTotal * 100) / 100);
  cy.get(EstimateLocators.sideClick).click();
  cy.wait('@updateJob').its('response.statusCode').should('eq', 200);

  // hours * (rate + markupValue)
  cy.get(EstimateLocators.valueToggleBtn + ':contains(' + data.markupType + ')')
    .eq(0)
    .click();
  cy.get(EstimateLocators.sideClick).click();
  // cy.get(EstimateLocators[type].markup).should("not.have.class", "Mui-disabled")
  cy.get(EstimateLocators[type].markup).clear().type(data.markup);
  let markupValue =
    data.markupType == '%' ? (data.rate * data.markup) / 100 : data.markup;
  lineTotal = data.hours * (data.rate + markupValue);
  cy.get(EstimateLocators[type].lineItemTotalAssertValue)
    .last()
    .should('have.value', Math.round(lineTotal * 100) / 100);
  cy.get(EstimateLocators.sideClick).click();
  cy.wait('@updateJob').its('response.statusCode').should('eq', 200);

  if (data.isTaxed) {
    // ( hours * (rate + markupValue) ) - discountedValue + taxedValue
    cy.performOperation('applyTax', 'applyTax', 'Estimate');
    cy.wait('@updateJob').its('response.statusCode').should('eq', 200);
    let taxedValue = (lineTotal * 8.7) / 100; // 8.7 is fixed, should be changed
    lineTotal = lineTotal + taxedValue;
    cy.get(EstimateLocators[type].lineItemTotalAssertValue)
      .last()
      .should('have.value', Math.round(lineTotal * 100) / 100);
    // cy.get(EstimateLocators[type].hours).should("not.have.class", "Mui-disabled")
  }
  // cy.get(EstimateLocators[type].lineItemTotalAssertValue).last().should("have.value", data.lineItemTotalAssertValue)

  // ( hours * (rate + markupValue) ) - discountedValue
  cy.get(
    EstimateLocators.valueToggleBtn + ':contains(' + data.discountType + ')',
  )
    .eq(1)
    .click();
  cy.get(EstimateLocators.sideClick).click();
  // cy.get(EstimateLocators[type].discount).should("not.have.class", "Mui-disabled")
  cy.get(EstimateLocators[type].discount).clear().type(data.discount);
  let discountedValue =
    data.discountType == '%'
      ? (lineTotal * data.discount) / 100
      : data.discount;
  lineTotal = lineTotal - discountedValue;
  cy.get(EstimateLocators[type].lineItemTotalAssertValue)
    .last()
    .should('have.value', Math.round(lineTotal * 100) / 100);
  cy.get(EstimateLocators.sideClick).click();
  cy.wait('@updateJob').its('response.statusCode').should('eq', 200);
  cy.get(EstimateLocators[type].hours).should('not.have.class', 'Mui-disabled');
  return lineTotal;
}

export function addItems(type, count, jobTotal) {
  cy.fixture('Estimate_data').then((data) => {
    let itemType = type == 0 ? 'labor' : 'part';
    cy.get(EstimateLocators[itemType].jobTotal).as('jobTotalCount');
    cy.log(jobTotal);
    for (let i = 0; i <= count; i++) {
      cy.get(EstimateLocators.jobSections)
        .eq(type)
        .within(() => {
          cy.get(EstimateLocators.addItemBtn).click();
          cy.wait('@updateJob').its('response.statusCode').should('eq', 200);
          cy.get(EstimateLocators.laborRows).should('have.length', i + 1);
          cy.get(EstimateLocators.laborRows)
            .eq(i)
            .within(() => {
              // Markup type = $|%  ||  Discount type = $|% | Tax type = O(No Tax) | Tax Class
              let lineTotal = fillAndAssertLineTotal(
                data['fillLaborCase' + i],
                i,
                itemType,
              );
              jobTotal = jobTotal + lineTotal;
              if (i == count) {
                cy.get('@jobTotalCount').should(
                  'contain',
                  (Math.round(jobTotal * 100) / 100).toString(),
                );
                // cy.get("@jobTotalCount").should("have.text", "$" + jobTotal.toString())
              }
            });
        });
    }
  });
}
