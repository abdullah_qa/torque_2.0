import { getEnvUrl } from '../commands';
const token = localStorage['token'];

export function addTaxClassesUsingApi() {
  cy.fixture('Settings_data').then((data) => {
    for (let item in data.taxClasses) {
      let name = data.taxClasses[item].data[0].name;
      cy.log(name);
      cy.request({
        method: 'GET',
        url: getEnvUrl() + 'tax-class/list',
        headers: {
          Connection: 'keep-alive',
          Accept: 'application/json, text/plain, */*',
          Authorization: 'Bearer ' + token,
          authority: 'apidevelop.torque360.co',
          Origin: Cypress.config().baseUrl,
          Referer: Cypress.config().baseUrl + '/',
        },
      }).then((response) => {
        expect(response.status).equal(200);
        if (response.body.data != []) {
          if (JSON.stringify(response.body.data).includes(name)) {
            cy.log('The Tax Class already exists.');
          } else {
            cy.log(JSON.stringify(response.body.data));
            cy.request({
              method: 'POST',
              url: getEnvUrl() + 'tax-class/create',
              headers: {
                Connection: 'keep-alive',
                Accept: 'application/json, text/plain, */*',
                Authorization: 'Bearer ' + token,
                authority: 'apidevelop.torque360.co',
                Origin: Cypress.config().baseUrl,
                Referer: Cypress.config().baseUrl + '/',
              },
              body: data.taxClasses[item],
            }).then((response) => {
              expect(response.status).equal(200);
              cy.log('the Tax Class created successfully.');
            });
          }
        } else {
          cy.request({
            method: 'POST',
            url: getEnvUrl() + 'tax-class/create',
            headers: {
              Connection: 'keep-alive',
              Accept: 'application/json, text/plain, */*',
              Authorization: 'Bearer ' + token,
              authority: 'apidevelop.torque360.co',
              Origin: Cypress.config().baseUrl,
              Referer: Cypress.config().baseUrl + '/',
            },
            body: data.taxClasses[item],
          }).then((response) => {
            expect(response.status).equal(200);
            cy.log('the Tax Class created successfully.');
          });
        }
      });
    }
  });
}

export function addLaborClassesUsingApi() {
  cy.fixture('Settings_data').then((data) => {
    for (let item in data.laborClasses) {
      let name = data.laborClasses[item]['className'];
      cy.log(name);
      cy.request({
        method: 'GET',
        url: getEnvUrl() + 'labourClass/get-labour-classes',
        headers: {
          Connection: 'keep-alive',
          Accept: 'application/json, text/plain, */*',
          Authorization: 'Bearer ' + token,
          authority: 'apidevelop.torque360.co',
          Origin: Cypress.config().baseUrl,
          Referer: Cypress.config().baseUrl + '/',
        },
      }).then((response) => {
        expect(response.status).equal(200);
        if (response.body.data != []) {
          if (JSON.stringify(response.body.data).includes(name)) {
            cy.log('The Labor Class already exists.');
          } else {
            cy.log(JSON.stringify(response.body.data));
            cy.request({
              method: 'POST',
              url: getEnvUrl() + 'labourClass/add',
              headers: {
                Connection: 'keep-alive',
                Accept: 'application/json, text/plain, */*',
                Authorization: 'Bearer ' + token,
                authority: 'apidevelop.torque360.co',
                Origin: Cypress.config().baseUrl,
                Referer: Cypress.config().baseUrl + '/',
              },
              body: data.laborClasses[item],
            }).then((response) => {
              expect(response.status).equal(200);
              cy.log('the Labor Class created successfully.');
            });
          }
        } else {
          cy.request({
            method: 'POST',
            url: getEnvUrl() + 'labourClass/add',
            headers: {
              Connection: 'keep-alive',
              Accept: 'application/json, text/plain, */*',
              Authorization: 'Bearer ' + token,
              authority: 'apidevelop.torque360.co',
              Origin: Cypress.config().baseUrl,
              Referer: Cypress.config().baseUrl + '/',
            },
            body: data.laborClasses[item],
          }).then((response) => {
            expect(response.status).equal(200);
            cy.log('the Labor Class created successfully.');
          });
        }
      });
    }
  });
}
