export default {
    toastMessage: ".submitConfirmContent p > p",
    validationError: "p.Mui-error",
    phoneValidationError: ".phoneRequire",
    navNewActivityBtn: "button.navbarBtn",
    navYourWorkflowBtn: ".WorkflowBtn.navbarBtn",
    navOptions: "[role='menuitem']",
    dropdownOptions: "[role='option']",
    submitButton: "#submit-login",
    dropdownOptionButtons: "[role='button']",
    dialogBtns: "[role='dialog'] button",
    emailField: "#email",
    name: "#name",
    errorUnderLine: ".MuiInput-underline.Mui-error",
    pageHeading: ".GarrageInfo.header__title__ro",
    submitConfirmText: ".SubmitConfirmText p",
    customerToastMessage: ".SubmitConfirmText p.para",
    navLinks: "a.nav__link"
}