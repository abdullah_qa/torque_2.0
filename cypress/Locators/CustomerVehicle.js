export default {
    customerInformation: {
        name: "#customerName",
        customerEmail_Unique: "#customer_email",
        Phone_Unique: "[placeholder='Phone']",
        address: "#customer_address",
        company: "#customer_company",
        customerNotes: "#notes"
    },
    customerInformationOld: {
        name: "#customerName",
        customerEmail_Unique: "#customer_email",
        Phone_Unique: "[placeholder='Phone']",
        address: "#customer_address",
        customerSSN: "#customer_ssn",
        customersGroupDropdown: "#customer_group",
        optionCGTextButton: "[role='option']",
        comments: "#customer_comments",
        company: "#customer_company",
        isDiscountCheckBtn: "#customer_discount",
        discountToggleBtn: ".toggle_btn_group_main button:nth-child(2)",
        discount: "#customer_percentage",
        isTaxCheckBtn: "#isTaxExempt",
        customerNotes: "#notes"
    },
    vehicleInformation: {
        vehicleModelYear: "#modelYear",
        vehicleMake: "#vehicle_make",
        vehicleModel: "#vehicle_model",
        vehicleTypeDropdown: "#mui-component-select-vehicleType",
        optionVTTextButton: "[role='option']",
        vehiclePlate_Unique: "#vehicle_plate",
        vehicleVin: "#vehicle_vin",
        vehicleColor: "#vehicle_color",
        vehicleImageHolder: "#vehicleImage_1_",
        vehicleCylinders: "#vehicle_cylinder",
        vehicleFuelTypeDropdown: "#fuelType",
        optionFTTextButton: "[role='option']",
        engineDisplacement: "#engine_displacement",
        vehicleLitersDropdown: "#displacement__type",
        optionVLTextButton: "[role='option']",
        engineTransmissionDropdown: "#engine_transmission",
        optionETTextButton: "[role='option']",
        vehicleOdometerUnitRadio: "input[name='odoMeter']",
        vehicleTraveled: "#vehicle_traveled",
        vehiclePurchase: "#vehicle_purchase",
        vehicleInsuranceProvider: "#vehicle_insurance_provider",
        vehicleInsuranceExpiry: "#vehicle_insurance_expiry",
        vehiclePolicyNumber: "#vehicle_policy_number",
        vehicleNotes: "#vehicle_notes",
        saveBtn: ".MuiGrid-justify-content-xs-flex-end.MuiGrid-grid-xs-9 button:nth-child(1)",
        successMsg: ".SubmitConfirmText .para",
        okSuccessBtn: ".submitConfirmButton "
    },
    customerInformationBeDisabled: {
        nameBeDisabled: "#customerName",
        emailBeDisabled: "#customer_email",
        phoneBeDisabled: "[placeholder='Phone']",
        addressBeDisabled: "#customer_address",
        companyBeDisabled: "#customer_company",
        customerNotesBeDisabled: "#notes"
    },
    vehicleTwoInformation: {
        vehicleColor: "#vehicle_color",
        vehicleImageHolder: "#vehicleImage_1_",
        vehicleTraveled: "#vehicle_traveled",
        vehiclePurchase: "#vehicle_purchase",
        vehicleInsuranceProvider: "#vehicle_insurance_provider",
        vehicleInsuranceExpiry: "#vehicle_insurance_expiry",
        vehiclePolicyNumber: "#vehicle_policy_number",
        vehicleNotes: "#vehicle_notes",
        saveBtn: ".MuiGrid-justify-content-xs-flex-end.MuiGrid-grid-xs-9 button:nth-child(1)",
        successMsg: ".SubmitConfirmText .para",
        okSuccessBtn: ".submitConfirmButton"
    },
    import: {
        importFileHolder: "div.dialog__content__inner input[acceptedfiles='.csv,text/*']"
    },
    vinLookup: {
        vinSearch: "input#vin_search",
        "@vinSearch": 200,
        vehiclePlate_Unique: "#vehicle_plate"
    },
    plateLookup: {
        plateNo: "input#license_lookup",
        stateLookupBtn: "#mui-component-select-state",
        stateTextButton: "li[role='option']",
        "@plateLookup": 200
    },
    moreVehiclesDropdown: "div.MuiCardContent-root h3 button",
    addAnotherVehicleBtn: "div.MuiGrid-spacing-xs-2 button.MuiButton-fullWidth",
    footerButtons: "div.MuiGrid-root button",
    addCustomerBtn: ".Buttons button.addCustomer",
    addNewVehicleBtn: "button.MuiButton-fullWidth",
    okBtn: ".submitConfirmAction button"
}
