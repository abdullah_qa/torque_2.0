export default {
    opens: {
        cardInvoiceIdentifier: "ul[data-rbd-droppable-id='column-4'] li.board__card__hover > div > div > a"
    },
    cardInvoiceIdentifierClickable: "div:nth-child(3) .MuiGrid-justify-content-xs-flex-end button",
    creates: {
        cardCreateInvoiceBtn: "div.MuiGrid-align-items-xs-center.MuiGrid-grid-xs-12 > div.MuiGrid-justify-content-xs-flex-end > button",
        successMsg: "#swal2-title",
        okSuccessBtn: ".CustomizeSwalConfirmButton"
    },
    jobCard: "li.board__card__hover",
    cardCreateInvoiceIdentifierClickable: "div:nth-child(4) > div > div > div > div:nth-child(2) .MuiGrid-justify-content-xs-flex-end button",
    previewFields: {
        type: "td:nth-child(3)",
        quantity: "td:nth-child(4)",
        unitPrice: "td:nth-child(5)",
        total: "td:nth-child(7)",
        discountInput: "div.discount__toggle__field input",
        selectedDiscountType: "button.discount__toggle__btn.Mui-selected",
        totalDiscount: "div.MuiGrid-justify-content-xs-flex-end.MuiGrid-grid-xs-12:nth-child(1) > div > p:nth-child(2) b",
        subTotal: "div.MuiGrid-justify-content-xs-flex-end.MuiGrid-grid-xs-12:nth-child(2) > div > p:nth-child(2) b",
        grandTotal: "div.MuiGrid-justify-content-xs-flex-end.MuiGrid-grid-xs-12:nth-child(5) > div > p:nth-child(2) b",
        taxTotalInput: "div.MuiGrid-justify-content-xs-flex-end.MuiGrid-grid-xs-12:nth-child(3) > div input",
        totalLaborTip: "div.MuiGrid-justify-content-xs-flex-end.MuiGrid-grid-xs-12:nth-child(4) > div input"
    },
    lineItem: "div.main__invoicing__table tr",
    fixedDiscountToggleBtn: "button.discount__toggle__btn[value='fixed']",
    percentageDiscountToggleBtn: "button.discount__toggle__btn[value='percentage']"
}