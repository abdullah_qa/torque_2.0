export default {
    creates: {
        cardPaymentCashBtn: "button[value='cash']:visible",
        amountPaid: "#paidAmount",
        cardAddPaymentBtn: ".MuiGrid-justify-content-xs-flex-end button:contains(Pay Now):visible",
        successMsg: "#swal2-title",
        okSuccessBtnForce: ".CustomizeSwalConfirmButton"
    },
    cardGenInvoiceBtn: "button[id='pdf_button']:visible",
    backBtn: "ul[data-rbd-droppable-id='column-4'] div.MuiGrid-align-items-xs-center.MuiGrid-grid-xs-12 button:visible",
    jobCard: "li.board__card__hover",
    cardPaymentCashIdentifierClickable: "button[value='cash']",
    cardAddPaymentIdentifierClickable: "div:nth-child(4) > div > div > div > div:nth-child(2) .MuiGrid-justify-content-xs-flex-end button",
    cardGenInvoiceIdentifierClickable: "#pdf_button"
}