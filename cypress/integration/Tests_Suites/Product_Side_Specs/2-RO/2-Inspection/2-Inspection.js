/// <reference types="Cypress" />

import 'cypress-file-upload'

const commonLocators = require("../../../../../Locators/commonLocators")
const InspectionLocators = require("../../../../../Locators/InspectionLocators")

describe("All the test cases of Inspction CRUD operations", () => {
    before(() => {
        cy.fixture("user-creds").then(data => {
            cy.loginWithApi(data.username, data.password)
            // cy.loginWithUI(data.username, data.password)
        })
        // cy.loginWithApi(Cypress.env("Username"), Cypress.env("Password"))
        // cy.loginWithUI(Cypress.env("Username"), Cypress.env("Password"))
        // Add more to before all operation.
        // cy.visit("/")
        // cy.get(commonLocators.pageHeading).should("contain", "Dashboard")
        // cy.socialLoginWithApi(Cypress.env("googleLogin"))
        cy.fixture("UniqueIds").then(ids => {
            cy.visit("/new-repairorder/" + ids.roId)
            // cy.wait("@getCreatedRO").its("response.statusCode").should("eq", 200)
            cy.get(commonLocators.pageHeading).should("have.text", "Repair Order: #RO-" + ids.roId.padStart(6, '0'))
        })
    })

    beforeEach(() => {
        // Add to each "it" 
    })

    // it("User should not be allowed to Generate DVI without entering any field.", () => {
    //     // Initially, the "disabledFields" should not be clickable at "Inspection" screen.
    //     cy.performOperation("disabledFields", "Inspection")
    // })

    it("Generate DVI by adding customer/vehicle but without general/detailed inspection.", () => {
        // the user selects an existing customer.
        // cy.addCustomerVehicle()
        cy.performOperation("createNew", "createNew", "Inspection")

        cy.get(InspectionLocators.generateBtn).should("be.disabled")
        // the user hits "generate" button without submitting mandatory fields at the "Inspection" screen.
        // cy.performOperation("generateInvalid", "generateInvalid", "Inspection")
    })

    it("Generate DVI General Inspection Template", () => {
        // the user hits the "generalInspectionBtn" button at "Inspection" screen.
        //     cy.get(InspectionLocators.generalInspectionBtn).click()

        //     // the user adds condition details for the "interiorExterior" inspection.
        //     cy.addInspection("interiorExterior")

        //     // the user hits the "saveBtn" button at "Inspection" screen.
        //     cy.get(InspectionLocators.saveBtn).click()

        //     cy.get(InspectionLocators.generateBtn).click()
        //     //  the user hits "generate" button with submitting all mandatory fields at the "Inspection" screen.
        //     // cy.performOperation("generateValid", "Inspection")

        //     // the user "authorizes" the "Inspection".
        //     cy.performOperation("authorizes", "authorizes", "Inspection")
        // })

        // it("Validate the added inspections, should show correct values.", () => {

        // })

        // it("Generate DVI by entering all fields.", () => {
        //     cy.reload()
        //     cy.performOperation("createNew", "createNew", "Inspection")
        //     // the user selects an existing customer.
        //     // cy.addCustomerVehicle()

        // the user hits the "generalInspectionBtn" button at "Inspection" screen.
        cy.get(InspectionLocators.generalInspectionBtn).click()

        // Add condition details for the "interiorExterior" inspection.
        cy.addInspection("interiorExterior")

        // Add condition details for the "underVehicle" inspection.
        cy.addInspection("underVehicle")

        // Add condition details for the "underHood" inspection.
        cy.addInspection("underHood")

        // Add condition details for the "electronics" inspection.
        cy.addInspection("electronics")

        // Hit the "saveBtn" button at "Inspection" screen.
        cy.get(InspectionLocators.saveBtn).click()
        cy.get(InspectionLocators.detailedInspectionBtn).should("be.disabled")
        // // Hit the "detailedInspectionBtn" button at "Inspection" screen.
        // cy.get(InspectionLocators.detailedInspectionBtn).click()

        // // Add condition details for the "detailSearch" inspection.
        // cy.addInspection("detailSearch")

        // // Hit the "saveBtn" button at "Inspection" screen.
        // cy.get(InspectionLocators.saveBtn).click()

        // Add "allFields" at the "Inspection" screen.
        cy.performOperation("allFields", "allFields", "Inspection")

        // And the user "authorizes" the "Inspection".
        cy.performOperation("authorizes", "authorizes", "Inspection")
    })

    it("Generate DVI Detailed Inspection Template", () => {
        // cy.reload()
        cy.performOperation("createNew", "createNew", "Inspection")
        // the user selects an existing customer.
        // cy.addCustomerVehicle()

        // Hit the "detailedInspectionBtn" button at "Inspection" screen.
        cy.get(InspectionLocators.detailedInspectionBtn).click()

        // Add condition details for the "detailSearch" inspection.
        cy.addInspection("detailSearch")

        // Hit the "saveBtn" button at "Inspection" screen.
        cy.get(InspectionLocators.saveBtn).click()

        cy.get(InspectionLocators.generalInspectionBtn).should("be.disabled")

        // Add "allFields" at the "Inspection" screen.
        cy.performOperation("allFields", "allFields", "Inspection")

        // And the user "authorizes" the "Inspection".
        cy.performOperation("authorizes", "authorizes", "Inspection")
    })

    it("Validate the add inspections, should show correct values.", () => {
        // cy.reload() // Bug
        // the "previewFields" appears for the created "Inspection" should be correct.
        cy.assertRecommendationsAdded()

        // the parts with "Parts Need Attention" or "moderate" conditions should show correct inspection results.
        // cy.assertInspectionPreview("Parts Need Attention", "moderate")

        // the parts with "Immediate Action Required" or "bad" conditions should show correct inspection results.
        // cy.assertInspectionPreview("Immediate Action Required", "bad")
    })
})