/// <reference types="Cypress" />

import { addItems } from '../../../../../support/BusinessActions/EstimateActions';
import {
  addLaborClassesUsingApi,
  addTaxClassesUsingApi,
} from '../../../../../support/BusinessActions/SettingsActions';

const commonLocators = require('../../../../../Locators/commonLocators');
const EstimateLocators = require('../../../../../Locators/EstimateLocators');

describe('All the test cases of Estimate CRUD operations', () => {
  before(() => {
    cy.fixture("user-creds").then(data => {
      cy.loginWithApi(data.username, data.password)
      // cy.loginWithUI(data.username, data.password)
    })
    // cy.loginWithApi(Cypress.env('Username'), Cypress.env('Password'));
    // Add more to before all operation.
    // cy.visit("/dashboard")
    // cy.get(commonLocators.pageHeading).should("contain", "Dashboard")
    // cy.selectFromMenu("Estimate")
    // cy.get(commonLocators.pageHeading).should("contain", "Estimate")
    // cy.socialLoginWithApi(Cypress.env("googleLogin"))
    cy.fixture('UniqueIds').then((ids) => {
      cy.visit('/new-repairorder/' + ids.roId);
      // cy.wait("@getCreatedRO").its("response.statusCode").should("eq", 200)
      cy.get(commonLocators.pageHeading).should(
        'have.text',
        'Repair Order: #RO-' + ids.roId.padStart(6, '0'),
      );
    });
    addLaborClassesUsingApi();
    addTaxClassesUsingApi();
  });

  beforeEach(() => {
    // Add to each "it"
  });

  it('Add Labor items to the Job and assert calculations.', () => {
    cy.get(EstimateLocators.estimateTabButton).click();
    cy.wait('@getJobsOfEstimate')
      .its('response')
      .then((getJobsResponse) => {
        expect(getJobsResponse.statusCode).to.eql(200);
        let jobTotal = getJobsResponse.body.data.ro.jobs[0].totalCost;
        cy.fixture('Estimate_data').then((data) => {
          cy.get(EstimateLocators.jobs)
            .eq(0)
            .within(() => {
              cy.get(EstimateLocators.jobToggle).click();
              // addItems
              // param 1 :: 0 = Labor, 1 = Parts and Supplies, 2 = Misc
              // param 2 :: No of line items

              addItems(0, 7, jobTotal); // Labor
              // addItems(1, 7) // Parts And Supplies
              // addItems(2, 7) // Misc
            });
        });
      });
  });
});
